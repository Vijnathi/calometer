package com.example.calometer;

import java.net.URL;

public class CredentialRestClient {
    private static final String BASE_URL =
            "http://192.168.18.46:8080/CalorieTrackerApp/webresources/";

    public static String findUser(String username, String passwordhash) {


        final String methodPath = "calorietracker.credential/findByUsernameandPassword/" + username + "/" + passwordhash;

        URL url = null;
        String textResult = "";

        try {
            url = new URL(BASE_URL + methodPath);
            textResult = GeneralHttpUrlConnection.getJsonDataFromURL(url, "GET");

            return textResult;
        }
        catch(Exception e){
            e.printStackTrace();
        }

        return textResult;
    }
}
