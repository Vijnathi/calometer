package com.example.calometer;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_main);

        ActionBar myActionBar = getSupportActionBar();

        //For hiding android actionbar
        myActionBar.hide();

        Button loginButton = (Button) findViewById(R.id.loginButton);
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                TextView uError =  ((TextView) findViewById(R.id.usernameError));
                TextView pError = ((TextView) findViewById(R.id.passwordError));
                TextView error = (TextView) findViewById(R.id.loginError);
                error.setText("");
                uError.setText("");
                pError.setText("");
                String username = ((EditText) findViewById(R.id.et_username)).getText().toString();
                String password = ((EditText) findViewById(R.id.et_password)).getText().toString();

                if(username.equals("")){
                    uError.setText("Enter Username");
                }
                if(password.equals("")){
                    pError.setText("Enter Password");
                }

                if (!username.equals("") && !password.equals("")) {

                    String passwordhash = "";
                    try {
                        // getInstance() method is called with algorithm SHA-1
                        MessageDigest md = MessageDigest.getInstance("SHA-1");

                        // digest() method is called
                        // to calculate message digest of the input string
                        // returned as array of byte
                        byte[] messageDigest = md.digest(password.getBytes());

                        // Convert byte array into signum representation
                        BigInteger no = new BigInteger(1, messageDigest);

                        // Convert message digest into hex value
                        passwordhash = no.toString(16);

                        // Add preceding 0s to make it 32 bit
                        while (passwordhash.length() < 32) {
                            passwordhash = "0" + passwordhash;
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    String[] credentials = {username, passwordhash};
                    LoginAsyncTask getUser = new LoginAsyncTask();
                    getUser.execute(credentials);
                }

            }
        });
    }

    private class LoginAsyncTask extends AsyncTask<String, Void, String>
    {
        @Override
        protected String doInBackground (String... params){
            return CredentialRestClient.findUser(params[0], params[1]);
        }
        @Override
        protected void onPostExecute (String user){
            if(user != null) {
                Intent intent = new Intent(MainActivity.this, HomeActivity.class);
                Bundle bundle = new Bundle();

                bundle.putString("user", user);
                intent.putExtras(bundle);
                startActivity(intent);
            } else {
                TextView error = (TextView) findViewById(R.id.loginError);
                error.setText("Invalid credentials");
//                EditText username = (EditText) findViewById(R.id.username);
            }

//            TextView resultTextView = (TextView) findViewById(R.id.tvResult);
//            resultTextView.setText(courses);
        }
    }
}
