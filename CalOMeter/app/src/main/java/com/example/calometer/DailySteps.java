package com.example.calometer;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity
public class DailySteps {

    @PrimaryKey(autoGenerate = true)
    public int dsid;

    @ColumnInfo(name = "steps")
    public int steps;

    @ColumnInfo(name = "time")
    public String timeEntered;

    public DailySteps(int steps, String timeEntered) {
        this.steps = steps;
        this.timeEntered = timeEntered;
    }

    public int getDsid() {
        return dsid;
    }

    public int getSteps() {
        return steps;
    }

    public void setSteps(int steps) {
        this.steps = steps;
    }

    public String getTimeEntered() {
        return timeEntered;
    }

    public void setTimeEntered(String timeEntered) {
        this.timeEntered = timeEntered;
    }
}
