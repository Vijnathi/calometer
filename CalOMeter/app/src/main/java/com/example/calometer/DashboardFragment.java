package com.example.calometer;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DashboardFragment extends Fragment {
    View vDashboard;
    TextView welcomeMessage;
    TextView time;
    TextView date;
    RelativeLayout layout;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle
            savedInstanceState) {
        vDashboard = inflater.inflate(R.layout.fragment_dashboard, container, false);
        welcomeMessage = (TextView) vDashboard.findViewById(R.id.welcomeMessage);
        layout = (RelativeLayout) vDashboard.findViewById(R.id.timings);
        time = (TextView) layout.findViewById(R.id.time);
        date = (TextView) layout.findViewById(R.id.date);
        displayWelcomeMessageWithTimings();
        return vDashboard;
    }

    public void displayWelcomeMessageWithTimings(){
        SharedPreferences sharedPreferencesUser =
                getActivity().getSharedPreferences("User", Context.MODE_PRIVATE);
        String user= sharedPreferencesUser.getString("user",null);

        Date today = new Date();
        SimpleDateFormat format = new SimpleDateFormat("MMM dd, yyyy hh:mm:ss a");
        String dateToStr = format.format(today);
        System.out.println(dateToStr);
        String message = "Good ";


        int start = dateToStr.length()-2;
        int end = dateToStr.length();
        String timeStr = dateToStr.substring(13,end);
        if((dateToStr.substring(start, end)).equalsIgnoreCase("AM")){
            message += "Morning, ";
        } else {
            int time = Integer.parseInt(dateToStr.substring(13, 15));
            if(time == 12 | (time > 1 & time < 5)){
                message += "Afternoon, ";
            } else {
                message += "Evening, ";
            }
        }
        String firstname = "";
        try{
            JSONObject jsonObject = new JSONObject(user);
            firstname = jsonObject.getJSONObject("userid").getString("firstname");
        } catch (Exception e) {
            e.printStackTrace();
        }
        message += firstname;
        welcomeMessage.setText(message);
        time.setText(timeStr);
        date.setText(dateToStr.substring(0,12));
    }
}

