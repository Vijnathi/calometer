package com.example.calometer;

import android.arch.persistence.room.Room;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class StepsFragment extends Fragment {
    View vSteps;
    DailyStepsDatabase db = null;
    EditText editText = null;
    Button save_btn = null;
    TableLayout tableLayout = null;
    int steps = 0;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle
            savedInstanceState) {
        vSteps = inflater.inflate(R.layout.fragment_steps, container, false);
        db = Room.databaseBuilder(getActivity().getApplicationContext(),
                DailyStepsDatabase.class, "CustomerDatabase")
                .fallbackToDestructiveMigration()
                .build();

        editText = (EditText) vSteps.findViewById(R.id.steps_input);
        save_btn = (Button) vSteps.findViewById(R.id.steps_save_button);

        ReadDatabase readSteps = new ReadDatabase();
        readSteps.execute();

        save_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InsertDatabase addSteps = new InsertDatabase();
                addSteps.execute();
            }
        });

        return vSteps;
    }

    private class InsertDatabase extends AsyncTask<Void, Void, String> {

        @Override
        protected String doInBackground(Void... params) {
            if(!(editText.getText().toString().isEmpty())) {
                steps = Integer.parseInt(editText.getText().toString());
                Date today = new Date();
                SimpleDateFormat format = new SimpleDateFormat("hh:mm:ss a");
                String dateToStr = format.format(today);

                System.out.println(dateToStr);

                DailySteps dailySteps = new DailySteps(steps, dateToStr);

                long id = db.dailyStepsDao().insert(dailySteps);

                return "Saved";
            } else {
                return "";
            }
        }

        @Override
        protected void onPostExecute(String message) {


            ReadDatabase readSteps = new ReadDatabase();
            readSteps.execute();
        }
    }

    private class ReadDatabase extends AsyncTask<Void, Void, List<DailySteps>> {
        @Override
        protected List<DailySteps> doInBackground(Void... params) {
            List<DailySteps> dailySteps = db.dailyStepsDao().getAll();
            if(!(dailySteps.isEmpty() || dailySteps == null)) {
                return dailySteps;
            }
            return null;
        }

        @Override
        protected void onPostExecute(List<DailySteps> dailySteps) {

            tableLayout = (TableLayout) vSteps.findViewById(R.id.dailyStepsTable);

            tableLayout.removeAllViews();
            for (final DailySteps steps: dailySteps){
                TableRow tableRow = new TableRow(getActivity());
                tableRow.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT,
                        TableRow.LayoutParams.WRAP_CONTENT, 1.0f));
                tableRow.setPadding(5, 5, 5, 5);
                tableRow.setGravity(Gravity.CENTER_VERTICAL);

                //steps display
                final EditText steps_value = new EditText(getActivity());
                TableRow.LayoutParams stepParams = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT,
                        TableRow.LayoutParams.WRAP_CONTENT, 1.0f);
                steps_value.setLayoutParams(stepParams);
                steps_value.setGravity(Gravity.CENTER);
                steps_value.setTextSize(20f);
                steps_value.setBackgroundResource(Color.alpha(0));
                steps_value.setTextColor(Color.parseColor("#000000"));
                steps_value.setText(String.valueOf(steps.getSteps()));


                //time display
                TextView time_value = new TextView(getActivity());
                TableRow.LayoutParams timeParams = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT,
                        TableRow.LayoutParams.WRAP_CONTENT, 1.0f);
                time_value.setLayoutParams(timeParams);
                time_value.setGravity(Gravity.CENTER);
                time_value.setTextSize(20f);
                time_value.setTextColor(Color.parseColor("#000000"));
                time_value.setText(steps.getTimeEntered());

                //edit button
                ImageButton imageButton = new ImageButton(getActivity());
                imageButton.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT,
                        TableRow.LayoutParams.WRAP_CONTENT, 1.0f));
                imageButton.setImageResource(R.drawable.edit);
                imageButton.setBackgroundColor(Color.alpha(0));
                imageButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String stepsEdited = steps_value.getText().toString();
                        String id = String.valueOf(steps.getDsid());
                        String time = steps.getTimeEntered();

                        String[] updateValues = {id, stepsEdited, time};
                        UpdateDatabase updateDatabase = new UpdateDatabase();
                        updateDatabase.execute(updateValues);
                    }
                });

                tableRow.addView(steps_value);
                tableRow.addView(time_value);
                tableRow.addView(imageButton);

                tableLayout.addView(tableRow);
            }
        }
    }

    private class UpdateDatabase extends AsyncTask<String, Void, String>{

        @Override
        protected String doInBackground(String... params) {
            DailySteps dailySteps = null;

            dailySteps = db.dailyStepsDao().findById(Integer.parseInt(params[0]));
            if (dailySteps != null) {
                dailySteps.setSteps(Integer.parseInt(params[1]));
                dailySteps.setTimeEntered(params[2]);

                db.dailyStepsDao().updateDailySteps(dailySteps);
                return "Success";
            }
            return "";

        }

        @Override
        protected void onPostExecute(String message) {
            ReadDatabase readDatabase = new ReadDatabase();
            readDatabase.execute();
        }
    }
}

