package com.example.calometer;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

public class SignupActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        Intent intent=getIntent();
        Bundle bundle=intent.getExtras();
        String user=bundle.getString("user");

        TextView t = (TextView) findViewById(R.id.text);

        String username = "";
        try{
            JSONObject jsonObject = new JSONObject(user);
            username = jsonObject.getString("username");
        } catch (Exception e) {
            e.printStackTrace();
//            username = "NO INFO FOUND";
        }

        t.setText(username);
    }
}
