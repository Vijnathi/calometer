package com.example.calometer;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.app.FragmentManager;
import android.app.Fragment;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.json.JSONObject;

public class HomeActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_home);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);

        drawer.addDrawerListener(toggle);
        toggle.syncState();
        toolbar.setNavigationIcon(R.drawable.hamburger_icon5);

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        ImageView homeImage = (ImageView) findViewById(R.id.fitnessHomeImage);
        homeImage.setVisibility(View.VISIBLE);

//        getSupportActionBar().setTitle("Navigation Drawer");
        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.content_frame, new DashboardFragment()).commit();

        Intent intent=getIntent();
        Bundle bundle=intent.getExtras();
        String user=bundle.getString("user");

        SharedPreferences sharedPreferencesUser = this.getSharedPreferences("User",
                Context.MODE_PRIVATE);

        SharedPreferences.Editor editSharedPreferencesUser = sharedPreferencesUser.edit();
        editSharedPreferencesUser.putString("user", user);
        editSharedPreferencesUser.apply();

        View headerView = navigationView.getHeaderView(0);
        TextView title = (TextView) headerView.findViewById(R.id.nav_header_title);
        TextView subtitle = (TextView) headerView.findViewById(R.id.nav_header_subtitle);

        String firstname = "";
        String lastname = "";
        String email = "";

        try{
            JSONObject jsonObject = new JSONObject(user);
            firstname = jsonObject.getJSONObject("userid").getString("firstname");
            lastname = jsonObject.getJSONObject("userid").getString("lastname");
            email = jsonObject.getJSONObject("userid").getString("email");
        } catch (Exception e) {
            e.printStackTrace();
//            username = "NO INFO FOUND";
        }

        title.setText(firstname +" "+lastname);
        subtitle.setText(email);

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        Fragment nextFragment = null;
        ImageView homeImage = (ImageView) findViewById(R.id.fitnessHomeImage);
        TextView title = (TextView) findViewById(R.id.toolbarTitle);
        RelativeLayout layout = (RelativeLayout) findViewById(R.id.content_home_layout);
        layout.setBackgroundColor(0x00000000);
        switch (id) {
            case R.id.nav_dashboard:
                homeImage.setVisibility(View.VISIBLE);
                title.setText("Cal - O - Meter");
                nextFragment = new DashboardFragment();
                break;

            case R.id.nav_dailyDiet:
                homeImage.setVisibility(View.GONE);
                title.setText("Daily Diet");
                layout.setBackgroundResource(R.color.background);
                nextFragment = new DailyDietFragment();
                break;

            case R.id.nav_steps:
                homeImage.setVisibility(View.GONE);
                title.setText("Steps");

                nextFragment = new StepsFragment();
                break;

            case R.id.nav_calorieTracker:
                homeImage.setVisibility(View.GONE);
                title.setText("Calorie Tracker");
                layout.setBackgroundResource(R.color.background);
                nextFragment = new CalorieTrackerFragment();
                break;

            case R.id.nav_report:
                homeImage.setVisibility(View.GONE);
                title.setText("Report");
                layout.setBackgroundResource(R.color.background);
                nextFragment = new ReportFragment();
                break;

            case R.id.nav_map:
                homeImage.setVisibility(View.GONE);
                title.setText("Map");
                layout.setBackgroundResource(R.color.background);
                nextFragment = new MapFragment();
                break;

            case R.id.nav_share:
                break;

            case R.id.nav_send:
                break;
        }

        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.content_frame, nextFragment).commit();

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


}
