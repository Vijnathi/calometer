package com.example.calometer;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

@Dao
public interface DailyStepsDao {

    @Insert
    long insert(DailySteps dailySteps);

    @Update(onConflict = REPLACE)
    public void updateDailySteps(DailySteps... dailySteps);

    @Query("SELECT * FROM dailysteps")
    List<DailySteps> getAll();

    @Query("SELECT * FROM dailysteps WHERE dsid = :dsid LIMIT 1")
    DailySteps findById(int dsid);
}
